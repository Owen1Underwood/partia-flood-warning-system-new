"""Work required for Task1D"""

from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station
from floodsystem.geo import station_list
from floodsystem.geo import stations_by_river 

def run():
    stations= build_station_list()
    c = station_list()
    x= rivers_with_station(c)
    b = x[:10]
    print(b)
    
    y = stations_by_river(stations)
    print(sorted(y['River Cam']))

    
run()