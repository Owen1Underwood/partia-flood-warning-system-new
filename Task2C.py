from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels

def run():
    stations = build_station_list()
    update_water_levels(stations)
    output = stations_highest_rel_level(stations, 10)
    for i in range(len(output)):
        print ("{}, {}".format(output[i][0].name, output[i][1]))
    
if __name__ == "__main__":
    print("*** Task 2C: CUED Part IA Flood Warning System) ***")


    run()
