import pytest
import datetime
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels
from floodsystem.flood import stations_highest_rel_level

def test_plot_water_level_with_fit():
    stations = build_station_list()
    update_water_levels(stations)
    
    stations_highest = stations_highest_rel_level(stations, 5)
    
    dt = 2
    
    for station in stations_highest:
        
        dates, levels = fetch_measure_levels(station[0].measure_id, datetime.timedelta(dt))
        
            
    assert len(dates) > 0 
    assert len(levels) > 0 
 
#def test_plot_water_levels():
        