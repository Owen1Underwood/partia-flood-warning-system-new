"Activities for task 2G"

import datetime
from floodsystem.flood import  stations_level_over_threshold_name_only
from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels
from floodsystem.utils import split_list, find_station, sorted_by_key
from floodsystem.datafetcher import fetch_measure_levels

def run():
    stations = build_station_list()
    update_water_levels(stations)
    stations_at_risk_today = stations_level_over_threshold_name_only(stations, 1)
    stations_at_risk_today = sorted_by_key(stations_at_risk_today, 1, reverse = True)
    #stations_at_risk_today = sorted(stations_at_risk_today)  
    high, mod, low = split_list([x[0] for x in stations_at_risk_today])
    dt = 3
    rising = []
    falling = []
    
    for station in stations_at_risk_today:
        current_station = find_station(stations, station[0])
        dates, levels = fetch_measure_levels(current_station.measure_id, datetime.timedelta(dt))
        day1, day2, day3 = split_list(levels)
        if day1 == [] or day2 == [] or day3 == []:
            pass
        else:
            av1 = (sum(day1)/ float(len(day1)))
            av2 = (sum(day2)/ float(len(day2)))
            av3 = (sum(day3)/ float(len(day3)))
            if av1 > av2 > av3:
                rising.append(station)
            elif av1 < av2 < av3 :
                falling.append(station)
            else:
                pass
    final_low = []
    final_mod = []
    final_high = []
    final_severe = []
        
            
    for station in low:
            if station in falling:
                pass
            elif station in rising:
                final_mod.append(station)
            else:
                final_low.append(station)
            
    for station in mod:
            if station in falling:
                final_low.append(station)
                
            elif station in rising:
                final_high.append(station)
            else:
                final_mod.append(station)
                
    for station in high:
            if station in falling:
                final_mod.append(station)
                
            elif station in rising:
                final_severe.append(station)
            else:
                final_high.append(station)
    
            
    print("stations at low risk", final_low)
    print("**************************")
    print("stations at moderate risk", final_mod)
    print("**************************")
    print("stations at high risk", final_high)
    print("**************************")
    print("stations at severe risk", final_severe)
    print("**************************")
    
    
    
   
        
               
                
        
    
    
    
if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System) ***")


    run()


