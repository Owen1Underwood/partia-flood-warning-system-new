# -*- coding: utf-8 -*-

from floodsystem.geo import stations_by_distance

from floodsystem.geo import station_list

def run():
    p = (52.2053, 0.1218)
    x = station_list()
    y = stations_by_distance(x, p)
    a = y[:10]
    b = y[-10:]
    c = a + b
    
    print(c)
    
run()
    
