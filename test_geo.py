import pytest
from floodsystem.geo import stations_within_radius
from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.geo import stations_by_distance
from floodsystem.geo import rivers_by_station_number

def test_stations_by_distance():
    p = (52.2053, 0.1218)
    stations = build_station_list()
    assert len(stations_by_distance(stations,p)) > 0

def test_stations_within_radius():
    stations = build_station_list()
    centre = (52.2053, 0.1218)
    r = 5
    assert len(stations_within_radius(stations, centre, r)) > 0 
    #for i in range(1000):
        #assert stations_within_radius(stations, centre, r)[i][1] <= r """
      
def test_rivers_with_station():
    stations = build_station_list()
    #rivers = rivers_with_station(stations)
    #for i in rivers:
        #if rivers == 'River Cam':
            #river_cam = rivers
            #break
    
    assert len(rivers_with_station(stations)) > 0

def test_stations_by_river():
    stations = build_station_list()
    assert len(stations_by_river(stations)) > 0 


def test_rivers_by_station_number():
    N = 5
    stations = build_station_list()
    assert len(rivers_by_station_number(stations, N)) > 0