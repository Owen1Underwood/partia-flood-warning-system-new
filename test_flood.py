# -*- coding: utf-8 -*-
from floodsystem.stationdata import build_station_list
from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level#Import all functions seperately from flood

stations = build_station_list()

def test_stations_level_over_threshold():
    """Tests function by asserting that the type is a LIST and that it contains TUPLES. Also double checks that it
    is in descending order"""
    test_list = stations_level_over_threshold(stations,0)
    assert type(test_list) == list 
    for i in range(0, len(test_list)):
        assert type(test_list[i]) == tuple
    for i in range(0, len(test_list)-1):
        assert test_list[i][1] >= test_list[i+1][1]            

