import numpy as np
import matplotlib
import datetime 

def polyfit(dates, levels, p):
    dates = matplotlib.dates.date2num(dates)
    
    if len(dates) == 0 or len(levels) == 0:
        
        poly = 0
        dates = None
        
    else:
        
        p_coeff = np.polyfit((dates- dates[0]), levels, p)
        
        poly = np.poly1d(p_coeff)
        
    return (poly, dates[0])
    
