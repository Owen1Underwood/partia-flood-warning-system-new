"""Submodule containing stuff related to plotting"""
import datetime
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from .analysis import polyfit
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
#type in the terminal 'python -m pip install matplotlib' to install the function.

def plot_water_levels(station, dates, levels):
    
        
    
    if len(dates) == 0 or len(levels) == 0:
        print("Not enough data available for {}".format(station[0].name))
    
    else:
        plt.plot(dates, levels, '.')
    
        x = np.ones(len(dates))
        y = (station[0].typical_range[0])*x
        z = (station[0].typical_range[1])*x

        plt.plot(dates, y, 'r-')
        plt.plot(dates, z, 'r-')
    
        plt.title("{}'s Water Level Data".format(station[0].name))
        plt.xlabel("Date")
        plt.ylabel("Water level /m")
        plt.xticks(rotation = 45)
        #plt.xlim(dates[0], dates[-1])
    
        plt.tight_layout()
        plt.show()

    

    
    
def plot_water_level_with_fit(station, dates, levels, p):
    
    poly, d0 = polyfit(dates, levels, p)
    
    dates = matplotlib.dates.date2num(dates)
    
    plt.plot(dates, levels, '.')
    
    x = np.ones(len(dates))
    y = (station[0].typical_range[0])*x
    z = (station[0].typical_range[1])*x     
    
    plt.plot(dates, poly(dates - d0))
    plt.plot(dates, y, 'r-')
    plt.plot(dates, z, 'r-')
    
    plt.title("{}'s Water Level Data".format(station[0].name))
    plt.xlabel("Time /days")
    plt.ylabel("Water level /m")
    #plt.xlim(dates[0], dates[-1])
    
    plt.tight_layout()
    plt.show()
    
