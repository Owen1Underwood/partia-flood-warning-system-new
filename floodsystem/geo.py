"""This module contains a collection of functions related to
geographical data.

"""
import numpy as np
from .utils import sorted_by_key
from .stationdata import build_station_list
from collections import Counter

from math import radians, cos, sin, asin, sqrt

AVG_EARTH_RADIUS = 6371 # in km

def haversine(point1, point2, miles=False):
    """ Calculate the great-circle distance between two points on the Earth surface.
    :input: two 2-tuples, containing the latitude and longitude of each point
    in decimal degrees.
    Example: haversine((45.7597, 4.8422), (48.8567, 2.3508))
    :output: Returns the distance bewteen the two points.
    The default unit is kilometers. Miles can be returned
    if the ``miles`` parameter is set to True.
    """
    # unpack latitude/longitude
    lat1, lng1 = point1
    lat2, lng2 = point2

    # convert all latitudes/longitudes from decimal degrees to radians
    lat1, lng1, lat2, lng2 = map(radians, (lat1, lng1, lat2, lng2))

    # calculate haversine
    lat = lat2 - lat1
    lng = lng2 - lng1
    d = sin(lat * 0.5) ** 2 + cos(lat1) * cos(lat2) * sin(lng * 0.5) ** 2
    h = 2 * AVG_EARTH_RADIUS * asin(sqrt(d))
    if miles:
        return h * 0.621371  # in miles
    else:
        return h # in kilometers

def station_list():
    stations = build_station_list()
    return stations
    
def stations_by_distance(stations, p):
    """This function creates a list of (station,distance) tuples where distance (float) is the distance of the monitoring station"""
    print("Number of stations: {}".format(len(stations)))
    stations_by_distance_list = []
    for station in stations:
        stations_by_distance_list.append((station.name, station.town, haversine(p, station.coord)))
    sorted_list = sorted_by_key(stations_by_distance_list,2)
    return(sorted_list)      
    
def stations_within_radius(stations, centre, r):
    """This fuction creates a list of stations within a certain radius of a point"""
    p = (52.2053, 0.1218)
    stations_by_distance_list = []
    for station in stations:
        stations_by_distance_list.append((station.name, station.town, haversine(p, station.coord)))
    sorted_list = sorted_by_key(stations_by_distance_list,2)
    within_radius = []
    for i in sorted_list:
        if i[2] <= r:
            within_radius.append(i[0])
        else:
            break
    within_radius_ordered = sorted_by_key(within_radius,0)
    return within_radius_ordered
    
def rivers_with_station(stations):
    """This function returns all rivers with a monitoring station given a list of stations"""
    p = (52.2053, 0.1218)
    stations_by_distance_list = []
    for station in stations:
        stations_by_distance_list.append((station.river, station.town, haversine(p, station.coord)))
    sorted_list = sorted_by_key(stations_by_distance_list,0)
    sorted_river = []
    for i in sorted_list:
        sorted_river.append(i[0])
    set_sorted_list = set(sorted_river)
    sorted_alphabet_river = sorted(set_sorted_list)
    print("Number of stations: {}".format(len(set_sorted_list))) 
    return sorted_alphabet_river
    
def stations_by_river(stations):
   
    """This function returns all monitoring stations on a given river"""
    #river_stations = []
    rivers= rivers_with_station(stations)
    river_and_stations={}
    stations_on_river=[]
    for river in rivers:
        
        for i in range(len(stations)):
            if stations[i].river==river:
                stations_on_river.append(stations[i].name)
            else:
                pass
        river_and_stations[river]= stations_on_river
        stations_on_river = []
    
    return river_and_stations

def rivers_by_station_number(stations, N): 
   """ This function returns a list of (river name, number of stations) tuples, sorted by the number of stations."""
   stations = build_station_list()
   rivers_list = []
   for station in stations:
       rivers_list.append(station.river)
     
   Num =  Counter(rivers_list).most_common()
   #Num2 = Num[:N+1]
   for i in range(100):
       if Num[N+i][1] == Num[i+N+1][1]:
           pass
       else:
           return Num[:N+i+1]
           break 
 
            # add the key to dictionaryKeys
              

    
if __name__ == "__main__":
    print("*** geo.py program ***")


 
    
    
        
    
    
    