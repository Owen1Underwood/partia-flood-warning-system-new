"""Sub module for function concerning floods"""
from .station import MonitoringStation
from .utils import sorted_by_key
#from .stationdata import build_station_list
#from floodsystem.station import MonitoringStation
#from floodsystem.stationdata import build_station_list
def stations_level_over_threshold(stations, tol):
    """Searches through a list of stations to find those with a current river level above a certain tolerane value. 
       It then adds these stations to a list, and then sorts it in decending order"""
  
    stations_over_tol = []
    
    for station in stations:
        
        if MonitoringStation.relative_water_level(station) == None:
            pass
        
        elif MonitoringStation.relative_water_level(station) > tol:
            stations_over_tol += [(station, MonitoringStation.relative_water_level(station))]
       
        else:
            pass
    
    stations_over_tol = sorted_by_key(stations_over_tol, 1, reverse = True)
    
    return stations_over_tol
    
def stations_level_over_threshold_name_only(stations, tol):
    """Searches through a list of stations to find those with a current river level above a certain tolerane value. 
       It then adds these stations to a list, and then sorts it in decending order"""
  
    stations_over_tol = []
    
    for station in stations:
        
        if MonitoringStation.relative_water_level(station) == None:
            pass
        
        elif MonitoringStation.relative_water_level(station) > tol:
            stations_over_tol += [(station.name, MonitoringStation.relative_water_level(station))]
       
        else:
            pass
    
    stations_over_tol = sorted_by_key(stations_over_tol, 1, reverse = True)
    
    return stations_over_tol
        
def stations_highest_rel_level(stations, N):
    """Takes a list of stations and returns a list of the N stations with the highest relative water level"""
    
    stations_high_waterlevel = stations_level_over_threshold(stations, 0)
    
    stations_high_waterlevel = stations_high_waterlevel[:N]

    return stations_high_waterlevel

            

