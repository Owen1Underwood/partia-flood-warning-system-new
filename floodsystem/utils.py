"""This module contains utility functions.

"""
import math

def sorted_by_key(x, i, reverse=False):
    """For a list of lists/tuples, return list sorted by the ith
    component of the list/tuple, E.g.

    Sort on first entry of tuple:

      > sorted_by_key([(1, 2), (5, 1]), 0)
      >>> [(1, 2), (5, 1)]

    Sort on second entry of tuple:

      > sorted_by_key([(1, 2), (5, 1]), 1)
      >>> [(5, 1), (1, 2)]

    """

    # Sort by distance
    def key(element):
        return element[i]

    return sorted(x, key=key, reverse=reverse)
    
def extract_name(stations):
    """Takes a list of stations and the name of one specific station and returns the corresponding monitoring stations"""
    new_list=[]
    for i in range(len(stations)):
        new_list.append(stations[i].name)
    return new_list
    
def find_station(stations, station_name):
    required_station= None
    for station in stations:
        if station.name == station_name:
            required_station = station
            break
    return(required_station)
    
def split_list(x):
    length = len(x)
    split_length = int(length/3)
    
    list1 = x[:split_length]
    list2 = x[split_length:split_length*2]
    list3 = x[-(length-split_length*2):]
          
    return(list1, list2, list3)
               
 
           
        
    
    
