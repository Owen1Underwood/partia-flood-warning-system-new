"""Work required for 1F"""

from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.utils import extract_name

def run():
    
    stations = build_station_list()
    inconsistent_list= inconsistent_typical_range_stations(stations)
    inconsistent_display= extract_name(inconsistent_list)
    inconsistent_display.sort()
    print(inconsistent_display)

run()

    