
import datetime
from floodsystem.flood import stations_highest_rel_level
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.plot import plot_water_levels

def run():
    
    stations = build_station_list()
    
    update_water_levels(stations)
    
    stations_highest = stations_highest_rel_level(stations, 5)
    
    dt = 10
    
    for station in stations_highest:
        
        dates, levels = fetch_measure_levels(station[0].measure_id, datetime.timedelta(dt))
        
        if len(dates) == 0:
            print("Not enough data available for {}".format(station[0].name))
        else:
            plot_water_levels(station, dates, levels)
            
if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")

    run()
