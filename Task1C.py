# -*- coding: utf-8 -*-
"""Work required for Task1C"""

from floodsystem.geo import stations_within_radius
from floodsystem.geo import station_list

def run():
    centre = (52.2053, 0.1218)
    
    x = station_list()
    r = 10
    
    y = stations_within_radius(x, centre, r)
    print(y)
    
run()