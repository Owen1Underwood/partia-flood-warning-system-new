"""Work required for 1E"""

from floodsystem.stationdata import build_station_list
from floodsystem.geo import station_list
from floodsystem.geo import rivers_by_station_number

def run():
    stations= build_station_list()
    c = station_list()
    
    a = rivers_by_station_number(c, 9)
    print(a)
    
    
run()